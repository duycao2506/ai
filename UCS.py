﻿import sys
import networkx as nx
import Algorithms
import time

class UCS(Algorithms.Algorithm):
	""" 
	--------------------------------------------------
	   Class for UCS algorithm
	--------------------------------------------------
	"""
	path_tracer = None
	
	def __init__(self,graph,start,end,updater):
		super(UCS, self).__init__(graph,start,end,updater)
		self.path_tracer = [-1]*graph.number_of_nodes()
		self.open = []
		self.start = start
		self.open.append((start,0))
		self.close = []
		self.graph = graph
		self.goal = []
		self.goal.append(end)
		self.ui_updater = updater


	# extract successor from a node
	def GetSuccessorsOf(self,node):
		return nx.neighbors(self.graph,node)

	# get edge weight from graph
	def GetEdgeWeight(self,node1,node2):
		a = nx.Graph(self.graph)
		return a.get_edge_data(node1,node2)['w']
	
	# check if node is in close
	def IsInClosed(self, node):
		for i in self.close:
			if i[0] == node:
				return i
		return None
	
	# check if node is in open 
	def IsInOpen(self,node):
		for i in self.open:
			if i[0] == node:
				return i
		return None

	# Find minimum cost in open
	def MinInOpen(self):
		#return self.open.pop(self.open.index(min(self.open,key = lambda t: t[1])))
		return self.open.pop(0)

	# Insert to the priority queue

	def InsertPriority(self,node):
		l = 0
		r = len(self.open)-1
		mid = 0
		while l <= r :
			mid = (l + r)/2
			if self.open[mid][1] > node[1]:
				r = mid - 1
			elif self.open[mid][1] < node[1]:
				l = mid + 1
			else:
				self.open.insert(mid,node)
				return
		if l < len(self.open):
			self.open.insert(mid,node)   
		else:
			self.open.append(node)
		#for i in range(len(self.open)):
		#	if self.open[i][1] >= node[1]:
		#		self.open.insert(i,node)
		#		return
		#self.open.append(node)

	# manipulate a successor
	def ManipSucc(self,nodeC,costC,isInClose,parent):
		cur_cost_c = nodeC[1]
		cost_c_new = min([costC,cur_cost_c])
		new_nodec = (nodeC[0],cost_c_new)
		if isInClose == False:
			self.open.remove(nodeC)
			#self.open.append(new_nodec)
			self.InsertPriority(new_nodec)
			if cost_c_new < cur_cost_c:
				self.path_tracer[new_nodec[0]] = parent
			
		if cost_c_new < cur_cost_c and isInClose:
			self.close.remove(nodeC)
			#self.open.append(nodeC)
			self.InsertPriority(new_nodec)
			self.path_tracer[new_nodec[0]] = parent

			#update color of node to open color : 'y'
			if self.ui_updater != None:
				self.ui_updater.ColorNode([nodeC[0]],self.time,'y',350)
				time.sleep(self.time)
		
		
	#Draw the path has been found
	def DrawPath(self):
		node1 = self.goal[0]
		node2 = -2
		while node2 != -1:
			node2 = self.path_tracer[node1]
			self.ui_updater.ColorPath(node1,node2,self.time)
			time.sleep(self.time)
			node1 = node2 
			if node1 == self.start:
				break
				

	def Run(self):
		while True :
			# if open is empty, return false ---> FAILED
			#print self.close
			
			
			if len(self.open) == 0:
				#update ui path
				return False

			# choose the minimum-cost state, save in close
			selection = self.MinInOpen()
		   
			self.close.append(selection)
			#update node color to close color 
			if self.ui_updater != None:
				self.ui_updater.ColorNode([selection[0]],self.time,'g',400)
				time.sleep(self.time)
			
			# if n is in goal, return True ---> SUCCESSFUL
			if selection[0] in self.goal:
				print self.close
				self.DrawPath() 
				return True
			
			#Generate successors of selection:
			successors = [x for x in self.GetSuccessorsOf(selection[0])]
			
			# filter and addition or movement
			for c in successors:
				cost_selection = selection[1]
				cost_selection_c= self.GetEdgeWeight(selection[0],c)
				cost_c = cost_selection + cost_selection_c        
				
				#Find c in open and closed
				tmpCl = self.IsInClosed(c)
				tmpOp = self.IsInOpen(c)
				
				
				new_node = None

				#Check if it is in Closed or Not to have a correct step

				#if tmpCl == None:
				#    if tmpOp == None:
				#        self.InsertPriority((c,cost_c))
				#    elif tmpOp != None and tmpOp[1] > cost_c:
				#        self.open.remove(tmpOp)
				#        self.InsertPriority((c,cost_c))
				#    self.path_tracer[new_node[0]] = selection[0]
				if tmpCl != None:
					new_node = tmpCl
					self.ManipSucc(new_node,cost_c,True,selection[0])
				elif tmpOp != None:
					new_node = tmpOp
					self.ManipSucc(new_node,cost_c,False,selection[0])
				else:
					new_node = (c,cost_c)
					#update node color 
					if self.ui_updater != None:    
						self.ui_updater.ColorNode([c],self.time,'y',350)
						time.sleep(self.time)
					#self.open.append(new_node)
					self.InsertPriority(new_node)
					self.path_tracer[c] = selection[0]
					if c==11:
						a = 0
					
	
		