﻿import sys
import GraphUI as grui
import wx
import wx.lib.scrolledpanel as sc
import wx.lib.intctrl as ictrl
import FakeData as fd
import Algorithms as al
import re

class Screen(object):
	""" 
	--------------------------------------------------
	   Class Screen of the program
	--------------------------------------------------
	"""
	app = None
	frame = None
	frameWidth = 0
	frameHeight = 0






class MainScreen(Screen):
	""" 
	--------------------------------------------------
	   Class MainScreen of the program
	--------------------------------------------------
	"""
	padding = 10
	choiceAl = None
	combo = None
	app = None
	input = []
	bSizer = None
	inputPanel = None
	inputScrollPanel = None
	edtNumCity = None
	rdGraphStyle = None
	rdGraphStyle2 = None
	cities = []

	def __init__(self):
		self.choiceAl = al.Algorithm.ALGOR
		self.app = wx.App(False)
		self.frame = wx.Frame(None, style = (wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN), title = "Path Finder")
		self.frameWidth = 700
		self.frameHeight = 600
		self.frame.Size = wx.Size(self.frameWidth,self.frameHeight)
		self.frame.Show(1)
		self.SetUpCombobox()
		self.SetupMenubar()
		self.SetUpNumCitiesInput()
		self.SetupRadioGraphStyle()
		self.bSizer = wx.BoxSizer(wx.VERTICAL)
		self.SetUpAddCityButton()
		self.SetUpInputPanel()
		self.app.MainLoop()
		return None



	#Combo box Setuper
	def SetUpCombobox(self):
		labelCombo = wx.StaticText(self.frame, label = StringLabel.LBL_ALGORITHM ,pos = (self.padding,self.padding),size = (100,20))
		comboPos = (100 + self.padding,self.padding / 2)
		comboSize = (self.frameWidth - 100 - 2 * self.padding,-1)
		self.combo = wx.ComboBox(self.frame,value = "UCS", choices = self.choiceAl,pos = comboPos , size = comboSize, style = wx.CB_READONLY)
		wx.EVT_COMBOBOX(self.frame,self.combo.GetId(),self.OnChangeCombobox)

	#set up menu bar
	def SetupMenubar(self):
		menuBar = wx.MenuBar()
		fileMenu = wx.Menu()
		openmenu = fileMenu.Append(wx.NewId(), "Load...",
									   "Load from text file")
		menuBar.Append(fileMenu, "&File")
		self.frame.Bind(wx.EVT_MENU, self.LoadFromFile, openmenu)
		self.frame.SetMenuBar(menuBar)

	#########################
	## Load file Section ####
	#########################
	def LoadFromFile(self, event):
		openFileDialog = wx.FileDialog(self.frame, "Open", "", "", 
									   "Text Files (*.txt)|*.txt", 
									   wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
		openFileDialog.ShowModal()
		filePath = openFileDialog.GetPath()
		openFileDialog.Destroy()
		if self.IsFileType(filePath):
			return
		# Read Algortihm
		algo = self.combo.GetStringSelection()
		isDirected = self.rdGraphStyle.GetValue()
		graph = self.ParseGraph(filePath,isDirected)
		nodes = graph.keys()
		haveHS = (algo == al.Algorithm.ALGOR[2])
		filename = filePath.split('.')[0]
		hs = [] if not haveHS else self.ParseHs(filename + "_hs.txt",nodes)
		self.configScreen = EdgesConfigScreen(nodes,algo,self.app,isDirected,graph,hs)
			 

	def ParseGraph(self,filePath,isDirected):
		content = []
		graph = {}
		fo = open(filePath, "r")
		lines = fo.read().splitlines()
		for i in range(len(lines)):
			tmparr = re.split(r'[\s\t]+',lines[i])
			cit1 = tmparr[0]
			cit2 = tmparr[1]
			if not isDirected:
				if not cit1 in graph.keys():
					graph[cit1] = {}
				if not cit2 in graph.keys():
					graph[cit2] = {}
		
				graph[cit2][cit1] = {}
				graph[cit1][cit2] = {}
				graph[cit1][cit2]['cost'] = graph[cit2][cit1]['cost'] = int(tmparr[2])	
			else:
				# Directed
				if graph[cit1] == None:
					graph[cit1] = {}
				graph[cit1][cit2] = {}
				graph[cit1][cit2]['cost'] = cost
		return graph

	def ParseHs(self,fileHsPath, nodes):
		arr = [-1]*len(nodes)
		fo = open(fileHsPath, "r")
		lines = fo.read().splitlines()
		for i in range(len(nodes)):
			tmparr = re.split(r'[\s\t]+',lines[i])
			index = nodes.index(tmparr[0])
			arr[index] = int(tmparr[1])
		return arr

	def IsFileType(self,path):
		tokens = path.split('.')
		return tokens[-1] == '.txt'

	###############################
	###############################
	###############################
	#Graph Style : directed or indirected
	def SetupRadioGraphStyle(self):
		self.rdGraphStyle = wx.RadioButton(self.frame, 
			label="Directed Graph", 
			pos = (self.combo.Position.x,self.edtNumCity.Size.GetHeight() + self.edtNumCity.Position.y + self.padding),
			size = (self.edtNumCity.Size.GetWidth(),20), name="graphstyle")
		self.rdGraphStyle2 = wx.RadioButton(self.frame, 
			label="Undirected Graph", 
			pos = (self.combo.Position.x + self.rdGraphStyle.Size.GetWidth(),self.rdGraphStyle.Position.y),
			size = (self.edtNumCity.Size.GetWidth(),20), name="graphstyle")
		self.rdGraphStyle2.SetValue(True)

	#input number of cities
	def SetUpNumCitiesInput(self):
		labelPos = (self.padding,self.combo.Size.GetHeight() + self.padding)
		labelNum = wx.StaticText(self.frame, label = StringLabel.LBL_NUM_CITY, pos = labelPos, size = (100,20))
		edtNumPos = (labelNum.Size.GetWidth() + self.padding, labelNum.Position.y)
		self.edtNumCity = ictrl.IntCtrl(self.frame, pos = edtNumPos) 


	
	# Input panel holds all the field that the users can input : cities' name,
	# distance or bird-flight distance.
	def SetUpInputPanel(self):
		rdHeight = self.rdGraphStyle.Size.GetHeight()
		position = wx.Point(0,self.rdGraphStyle.Position.y + rdHeight + self.padding)
		size = wx.Size(self.frameWidth - 5, self.frameHeight - rdHeight - 4.5 * self.padding - self.rdGraphStyle.Position.y)
		self.inputPanel = sc.ScrolledPanel(self.frame,pos = position,size = size,style = wx.SIMPLE_BORDER)
		self.inputPanel.SetSizer(self.bSizer)
		self.inputPanel.SetupScrolling(scroll_x=False,scroll_y=True)


	
	# Button which add the text field to input the cities
	def SetUpAddCityButton(self):
		position = wx.Point(self.edtNumCity.Size.GetWidth() + 100 + 2 * self.padding, self.edtNumCity.Size.GetHeight() + self.padding)
		btnSize = wx.Size(self.frame.Size.GetWidth() - position.x - self.padding,-1)
		btnAddCity = wx.Button(self.frame,label = StringLabel.BTN_INPUT_CITY ,pos = position ,size = btnSize)
		btnAddCity.Bind(wx.EVT_BUTTON,self.OnClickButtonAddCity)


	   
	# Panel holds input for a node
	def AddInput(self):
		self.input.append(self.InputCityField(self.inputPanel))
		#Key code to make scroller automatically resize
		self.inputPanel.FitInside()



	#Add button Config Edges (OK)
	def SetUpButtonConfig(self):
		lastComp = self.input[len(self.input) - 1].Parent
		position = wx.Point(self.frameWidth / 2 - 100,lastComp.Position.y + lastComp.Size.GetHeight() + 2 * self.padding)
		button = wx.Button(self.inputPanel, label = StringLabel.BTN_CONFIG_EDGES, pos = position, size = (200,30))
		
		self.bSizer.Add(button,0, wx.ALIGN_CENTRE_HORIZONTAL, 5)
		self.inputPanel.FitInside()
		button.Bind(wx.EVT_BUTTON,self.OnclickButtonConfig)
		
		return button



	# REturn the edit box of city name
	def InputCityField(self, parent):
		newY = 0
		if len(self.input) > 0:
			newY = self.input[len(self.input) - 1].GetPosition().y + 40
		panel = wx.Panel(parent,pos = (0,newY), size = (self.frame.Size.GetWidth(),40),style = wx.NO_BORDER)
		
		label = wx.StaticText(panel,label = StringLabel.LBL_CITY_NAME,pos = (self.padding,self.padding), size = (100,-1))
		edBox1 = wx.TextCtrl(panel,pos = (label.Size.GetWidth() + 2 * self.padding, label.Position.y),size = (200,-1))
		edBox1.IsSingleLine = True
		self.bSizer.Add(panel,0,wx.ALL,5)
		return edBox1

	

	"""Events"""
	# Event for changin combobox value
	def OnChangeCombobox(self,event):
		#self.AddInput()
		#print self.combo.GetStringSelection(), "GOOD"
		pass


	#Event Click Add City Button
	def OnClickButtonAddCity(self, event):
		#check if there is no algorithm has been selected
		if self.combo.GetSelection() == wx.NOT_FOUND :
			 wx.MessageBox(StringLabel.MSG_CHOOSE_AL, StringLabel.TITL_ALERT,  wx.OK | wx.ICON_WARNING)
			 return
		#Validate the value
		if self.edtNumCity.Validate() == False:
			 wx.MessageBox(StringLabel.MSG_INVALID_CITY_NUM, StringLabel.TITL_ALERT,  wx.OK | wx.ICON_WARNING)
			 return
		self.SetUpCityInput()


	
	#input view of cities
	def SetUpCityInput(self):
		# Check if there is input in the panel already ( refresh for new value
		# )
		lengInput = len(self.input)
		if lengInput > 0:
			#remove in boxsizer
			for x in range(lengInput):
				self.bSizer.Remove(0)
			#remove in panel
			for x in self.inputPanel.GetChildren():
				x.Destroy()
			self.inputPanel.FitInside()
			del self.input[:]
		
		numberofCity = int(self.edtNumCity.Value)
		
		for i in range(numberofCity) :
			self.AddInput()
		self.SetUpButtonConfig()



	#Event when clicking button config
	def OnclickButtonConfig(self, event):
		self.cities = []
		rnge = len(self.input)
		for i in range(rnge):
			string = self.input[i].Value
			if len(string) == 0:
				return
			self.cities.append(string)
		self.configScreen = EdgesConfigScreen(self.cities,self.combo.GetStringSelection(), self.app, self.rdGraphStyle.GetValue())











class EdgesConfigScreen(Screen):
	""" 
	--------------------------------------------------
	   Class holding Second Screen
	--------------------------------------------------
	"""
	mainPanel = None
	hsPanel = None
	
	hs = []

	cities_dict = {}
	padding = 10
	inputWindows = None
	cities = None
	algorithm = ""
	fromCombo = None
	toCombo = None
	#View
	speedCtrl = None
	isDirected = False
	startnode = 0
	endnode = 1
	def __init__(self, *args, **kwargs):
		return super(EdgesConfigScreen, self).__init__(*args, **kwargs)
	
	"""
	def __init__(self, data, algorithm, app):
		 self.cities = data
		 self.cities_dict = {}
		 self.inputWindows = []
		 self.CreateCitiesDict(data)

		 self.algorithm = algorithm
		 self.frame = wx.Frame(None, style = (wx.MINIMIZE_BOX|wx.SYSTEM_MENU|
				  wx.CAPTION|wx.CLOSE_BOX|wx.CLIP_CHILDREN), title = "Edges Config")
		 self.frameHeight = 768
		 self.frameWidth  = 1024
		 self.frame.Size = wx.Size(self.frameWidth,self.frameHeight)
		 self.SetUpControlPanel(False)
			
		 self.frame.Show()
	"""
	def __init__(self, cities, algorithm, app, isDirected, dict={}, hs=[]):
		 self.cities = cities
		 
		 self.isDirected = isDirected
		 self.inputWindows = []
		
		 if not dict:
			 self.CreateCitiesDict(cities)
		 else:
			self.cities_dict = dict

		 self.hs = hs
		 self.algorithm = algorithm
		 self.frame = wx.Frame(None, style = (wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.CLIP_CHILDREN), title = "Edges Config")
		 self.frameHeight = 768
		 self.frameWidth = 1024
		 self.frame.Size = wx.Size(self.frameWidth, 150 if dict else self.frameHeight)
		 self.SetUpControlPanel(dict)
		 self.SetUpHeurPanel(self.frame)
		 self.frame.Show()
		 self.startnode = 0
		 self.endnode = len(self.cities) -1 
	
	# Initialize a new dictionary from an array of cities.
	def CreateCitiesDict(self, data):
		for x in data:
			self.cities_dict[x] = {}



	# Control Panel with button more and button go
	def SetUpControlPanel(self,isLoadedfromFile):
		panelSize = wx.Size(self.frameWidth - 5 if self.algorithm != "A*" else self.frameWidth - 300,
							(0 if isLoadedfromFile else (self.frameHeight * 5 / 6))) 
		self.mainPanel = sc.ScrolledPanel(self.frame, 
										  pos = (0,0), size = panelSize)
		self.mainPanel.SetSizer(wx.BoxSizer(wx.VERTICAL))
		self.mainPanel.SetupScrolling(scroll_x=False,scroll_y=True)

		btnMorePos = wx.Point(2 * self.padding,
							  self.mainPanel.Size.GetHeight() + 2 * self.padding)
		btnMore = wx.Button(self.frame, pos = btnMorePos, 
							size = (100,(self.frameHeight / 6) - 6 * self.padding), 
							label = StringLabel.BTN_MORE)
		btnMore.Bind(wx.EVT_BUTTON,
					 self.SetUpEdgeInput)

		btnGoPos = wx.Point(btnMore.Size.GetWidth() + 3 * self.padding , 
							btnMorePos.y)
		btnGo = wx.Button(self.frame, 
						  pos = btnGoPos, 
						  size = (100,
								  (self.frameHeight / 6) - 6 * self.padding),
						  label = StringLabel.BTN_GO)
		btnGo.Bind(wx.EVT_BUTTON,self.OnClickButtonGo)
		self.SetupSpeedControl()
		self.SetupStartEndPoint(self.frame)
	   
		

	def SetupStartEndPoint(self, panel):
		fromText = wx.StaticText(panel,
								 pos = (self.speedCtrl.Position.x + self.speedCtrl.Size.GetWidth() + 4 * self.padding,self.speedCtrl.Position.y), 
								 size = (50,20),
								 label = StringLabel.FROM_TEXT)
		self.fromCombo = wx.ComboBox(panel,
									choices = self.cities, 
									value = self.cities[self.startnode],
									pos = (fromText.Position.x + 100 + 2 * self.padding,fromText.Position.y), 
									size = (100,20), 
									style = wx.CB_READONLY) 
		toText = wx.StaticText(panel,
							   pos = (self.fromCombo.Size.GetWidth() + self.fromCombo.Position.x + self.padding,self.fromCombo.Position.y), 
							   size = (50,20), label = StringLabel.TO_TEXT)
		self.toCombo = wx.ComboBox(panel,
									choices = self.cities,
									value = self.cities[self.endnode], 
									pos = (toText.Size.GetWidth() + toText.Position.x + self.padding,toText.Position.y), 
									size = (100,20), 
									style = wx.CB_READONLY)


	def SetupSpeedControl(self):
		speedCtrlPos = wx.Point(100 * 2 + 5 * self.padding,
								self.mainPanel.Size.GetHeight() + 3 * self.padding)    
		self.speedCtrl = wx.Slider(self.frame,
								   1500, 
								   1000, 
								   0, 
								   1500, 
								   pos=speedCtrlPos,
								   size=(250, -1),
								   style=wx.SL_HORIZONTAL | wx.SL_AUTOTICKS | wx.SL_LABELS ,name = "Speed Control")
		self.speedCtrl.Bind(wx.EVT_SLIDER,
							self.OnChangeSliderSpeed)


	####################
	# HS is an optional input so let it be in a panel
	def paddinngPanel(self,parent):
		paddinepanel = wx.Panel(parent,
						 pos = (0,parent.Position.y + parent.Size.GetHeight()), 
						 size = (12,self.padding))
		parent.GetSizer().Add(paddinepanel,0, wx.ALIGN_CENTRE_HORIZONTAL,5)
		parent.FitInside()
	
	def SetUpHeurPanel(self, parent):
		panelPos = wx.Point(self.mainPanel.Size.GetWidth() + 0.5 * self.padding, 
							0)
		panel = sc.ScrolledPanel(parent,
						 pos = panelPos, 
						 size = (parent.Size.GetWidth() - panelPos.x - 0.5 * self.padding,
								 self.mainPanel.Size.GetHeight()))
		panel.SetSizer(wx.BoxSizer(wx.VERTICAL))
		panel.SetupScrolling(scroll_x=False,scroll_y=True)
		
		

		self.hsPanel = panel
		self.paddinngPanel(self.hsPanel)
		labelHS = wx.StaticText(panel,
								id = 0,
								pos = (panel.Size.GetWidth() / 2 - 50,10),
								size = (100,20),
								label = StringLabel.LBL_HS,
								style = wx.ALIGN_CENTRE_HORIZONTAL)
		self.hsPanel.GetSizer().Add(labelHS,0,wx.ALIGN_CENTRE_HORIZONTAL,15)
		self.hsPanel.FitInside()
		for a in range(1,len(self.cities) + 1):
			labelHS1 = wx.StaticText(panel,
								id = 0,
								pos = (20,labelHS.Position.y + labelHS.Size.GetHeight() + self.padding + a * (40 + 2 * self.padding)),
								size = (200,20),
								label = self.cities[a - 1])
			self.hsPanel.GetSizer().Add(labelHS1,0, wx.ALIGN_CENTRE_HORIZONTAL,5)
			self.hsPanel.FitInside()
			editHS = ictrl.IntCtrl(self.hsPanel,
								value = -1,
								id = a,
								pos = (40,labelHS1.Position.y + labelHS1.Size.GetHeight() + self.padding), 
								size = (100 - self.padding,20))
			self.hsPanel.GetSizer().Add(editHS,0,wx.ALIGN_CENTRE_HORIZONTAL,15)
			self.hsPanel.FitInside()
			self.paddinngPanel(self.hsPanel)
		return panel


	########################
	########################

	# Panel for main input
	def SetUpEdgeInput(self,event):
		col = 6
		if self.algorithm == "A*":
			col = 8
		y = self.padding * 2
		l = len(self.inputWindows)
		
		if l > 0:
			lastY = self.inputWindows[l - 1].Position.y
			y = lastY + self.inputWindows[l - 1].Size.GetHeight() + 2 * self.padding
		
		panelpos = wx.Point(0,y)
		
		panelSize = wx.Size(self.frameWidth, 25)
		
		panel = wx.Panel(self.mainPanel,
						 pos = panelpos, 
						 size = panelSize)
		
		self.mainPanel.GetSizer().Add(panel,0,wx.ALL,5)
		
		self.mainPanel.FitInside()
		cellSize = wx.Size((self.frameWidth - (col + 1) * self.padding - 100) / col , 
						   panelSize.GetHeight())
		fromText = wx.StaticText(panel,
								 pos = (self.padding,0), 
								 size = cellSize,
								 label = StringLabel.FROM_TEXT)
		fromCombo = wx.ComboBox(panel,
									id = 4,
									choices = self.cities, 
									pos = (cellSize.GetWidth() + 2 * self.padding,0), 
									size = cellSize, 
									style = wx.CB_READONLY) 
	   
		toText = wx.StaticText(panel,
							   pos = (2 * cellSize.GetWidth() + 3 * self.padding,0), 
							   size = cellSize, label = StringLabel.TO_TEXT)
		toCombo = wx.ComboBox(panel,
									id = 5,
									choices = self.cities, 
									pos = (3 * cellSize.GetWidth() + 4 * self.padding,0), 
									size = cellSize, 
									style = wx.CB_READONLY)
		
		#IDS does not have weight consideration
		if self.algorithm != 'IDS':
			costText = wx.StaticText(panel,
								 pos = (4 * cellSize.GetWidth() + 5 * self.padding,0), 
								 size = cellSize, label = StringLabel.COST)
			costEdt = ictrl.IntCtrl(panel,
								value = -1,
								id = 6,
								pos = (5 * cellSize.GetWidth() + 6 * self.padding,0), 
								size = cellSize)
		
		#if self.algorithm == "A*":
		#    self.SetUpHeurDis(panel,cellSize)
		self.inputWindows.append(panel)




	#Find it, onclick btn GO
	def OnClickButtonGo(self, event):
		if len(self.inputWindows) > 0:
			#self.CreateCitiesDict(self.cities)
			for i in range(len(self.inputWindows)):
				self.GetDataToDictFromPanel(self.inputWindows[i])
			
		print self.cities_dict
		self.startnode = self.fromCombo.GetSelection()
		self.endnode = self.toCombo.GetSelection()
		if self.algorithm == "A*" and len(self.inputWindows) > 0:
			for i in range(1,len(self.cities) + 1):
				hsedt = self.hsPanel.FindWindowById(i)
				hsv = int(hsedt.GetValue())
				self.hs.append(hsv)
		
		# Fake data are being used here, need to be changed
		#self.graphuis =
		#grui.PathFinderGraph(self.cities,self.cities_dict,self.algorithm)
		self.graphuis = grui.PathFinderGraph(self.cities,self.cities_dict,self.algorithm,self.startnode,self.endnode,self.isDirected,self.hs)
		self.graphuis.DrawGraph(self.speedCtrl.GetValue())


	
	def OnChangeSliderSpeed(self,event):
		val = self.speedCtrl.GetValue()
		self.graphuis.SetSpeed(val)



	#Collecting data
	def GetDataToDictFromPanel(self,x):
		#hsBool = self.algorithm == "A*"
		#hs = -1
	   
		fromCombo = x.FindWindowById(4)
		toCombo = x.FindWindowById(5)
		costText = x.FindWindowById(6)
		fromX = fromCombo.GetSelection()
		toY = toCombo.GetSelection()
		cost = -1
		if self.algorithm != al.Algorithm.ALGOR[1]:
			cost = int(costText.Value)

		if fromX == wx.NOT_FOUND or toY == wx.NOT_FOUND or (self.algorithm != al.Algorithm.ALGOR[1] and cost == -1) :
			return
	   
		cit1 = self.cities[fromX]
		cit2 = self.cities[toY]
		#Undirected Graph
		if not self.isDirected:
			if self.cities_dict[cit1] == None:
					self.cities_dict[cit1] = {}
			if self.cities_dict[cit2] == None:
				self.cities_dict[cit2] = {}
		
			self.cities_dict[cit2][cit1] = {}
			self.cities_dict[cit1][cit2] = {}
		
			self.cities_dict[cit1][cit2]['cost'] = self.cities_dict[cit2][cit1]['cost'] = cost	
		else:
			# Directed
			if self.cities_dict[cit1] == None:
				self.cities_dict[cit1] = {}
			self.cities_dict[cit1][cit2] = {}
			self.cities_dict[cit1][cit2]['cost'] = cost
					  

	   
		








class StringLabel(object):
	""" 
	--------------------------------------------------
	   Class holding static final String and constant
	--------------------------------------------------
	"""
	
	#MainScreen
	LBL_ALGORITHM = "Algorithm: "
	LBL_NUM_CITY = "Number of City: "
	LBL_CITY_NAME = "City Name: "
	BTN_INPUT_CITY = "Input City"
	BTN_CONFIG_EDGES = "Config Edges"
	CB_DEFAULT = "---None---"
	MSG_CHOOSE_AL = "Choose Algorithm"
	TITL_ALERT = "Alert"
	MSG_INVALID_CITY_NUM = "Invalid Number of Cities"
	

	#Second Screen
	BTN_GO = "GO!"
	BTN_MORE = "MORE..."
	LBL_HS = "Heuristic Distance"
	TO_TEXT = "TO"
	FROM_TEXT = "FROM"
	COST = "COST"