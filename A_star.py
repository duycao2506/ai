# -*- coding: utf-8 -*-
"""
Created on Wed Apr 06 18:24:07 2016

@author: DELL
"""

import Algorithms
import networkx as nx
import time

"""
class A_star(Algorithms.Algorithm):
	 
	--------------------------------------------------
	   Class for A* algorithm
	-----
	


	path_tracer = None

	def __init__(self, graph, start, end, updater, straight_distance):
		super(A_star, self).__init__(graph, start, end, updater)
		self.path_tracer = [-1] * graph.number_of_nodes()
		self.open = []
		self.start = start
		self.open.append((start,0))
		self.close = []
		self.graph = graph
		self.goal = []
		self.goal.append(end)
		self.ui_updater = updater
		self.straight_distance = straight_distance
		self.hs = self.GetStraightWeight(start)

	def GetSuccessorsOf(self,node):
		return nx.neighbors(self.graph,node)

	# get edge weight from graph
	def GetEdgeWeight(self,node1,node2):
		a = nx.Graph(self.graph)
		return a.get_edge_data(node1,node2)['w']
	
	# check if node is in close
	def IsInClosed(self, node):
		for i in self.close:
			if i[0] == node:
				return i
		return None
	
	# check if node is in open
	def IsInOpen(self,node):
		for i in self.open:
			if i[0] == node:
				return i
		return None

	def MinInOpen(self):
		return self.open.pop(self.open.index(min(self.open,key = lambda t: t[1])))

	#Draw the path has been found
	def DrawPath(self):
		node1 = self.goal[0]
		node2 = -2
		while node2 != -1:
			node2 = self.path_tracer[node1]
			self.ui_updater.ColorPath(node1,node2,self.time)
			time.sleep(self.time)
			node1 = node2

	def GetStraightWeight(self, node):
		return self.straight_distance[node-1]

	def Run(self):
		while True:
			# Fail: If OPEN = {}, Terminate & Report Failure
			if len(self.open) == 0:
				return False

			# Select: Select the minimum cost start, n, from OPEN.  Save n in CLOSED
			selection = self.MinInOpen()
			self.close.append(selection)

			if self.ui_updater != None:
				self.ui_updater.ColorNode([selection[0]], self.time, 'g', 400)
				time.sleep(self.time) 

			# Terminate: If n in G, terminate with success and return f(n)
			if selection[0] in self.goal:
				self.DrawPath()
				return True
			
			
			
			#Generate successors of select
			successors = [x for x in self.GetSuccessorsOf(selection[0])]

			#filter and addition or movement
			for m in successors:
				cost_selection = selection[1]# = g(n)
				cost_selection_m = self.GetEdgeWeight(selection[0], m)  # = C(n,m)
				tmpCl = self.IsInClosed(m)
				tmpOp = self.IsInOpen(m)
				old_hs = self.hs

				# 1.  If m NOT in [OPEN U CLOSED]
				if tmpCl == None and tmpOp == None:
					# 1.  Set g(m) = g(n) + C(n,m)
					cost_m = cost_selection + cost_selection_m
					# 2.  set f(m) = g(m) + H(m)
					self.hs = cost_selection + self.GetStraightWeight(m)
					# 3.  insert m in OPEN
					self.open.append((m, cost_m)

				# 2.  If m in [OPEN U CLOSED]
				if tmpCl != None or tmpOp != None:
					# 1.  Set g(m) = min{g(m), g(n) + C(n, m)}
					if cost_m > cost_selection + cost_selection_m:
						cost_m = cost_selection + cost_selection_m
					# 2.  Set f(m) = g(m) + h(m)
					self.hs = cost_m + self.GetStraightWeight(m)

				# 3.  If f(m) has decreased and m in CLOSED
				if self.hs < old_hs and tmpCl != None:
                        self.close.append(self.open.pop(self.open.index(tmpOp)))
     """

                   
class A_star(Algorithms.Algorithm):
    patr_tracer = None
    
    def __init__(self, graph, start, end, updater, straight_distance):
        super(A_star, self).__init__(graph, start, end, updater)
        self.path_tracer = [-1] * graph.number_of_nodes()
        self.open = []
        self.start = start
        self.open.append((start,0))
        self.close = []
        self.graph = graph
        self.goal = []
        self.goal.append(end)
        self.ui_updater = updater
        self.straight_distance = straight_distance
        self.hs = self.GetStraightWeight(start)
        
    def GetSuccessorsOf(self, node):
        return nx.neighbors(self.graph, node)
        
    def GetEdgeWeight(self, node1, node2):
        a = nx.Graph(self.graph)
        return a.get_edge_data(node1, node2)['w']
        
    def IsInClosed(self, node):
        for i in self.close:
            if i[0] == node:
                return i
        return None
        
    def IsInOpen(self, node):
        for i in self.open:
            if i[0] == node:
                return i;
        return None
        
    def MinInOpen(self):
        return self.open.pop(0)
        
    def DrawPath(self):
        print(self.path_tracer)
        node1 = self.goal[0]
        node2 = -2
        while node2 != -1 :
            node2 = self.path_tracer[node1]
            self.ui_updater.ColorPath(node1, node2, self.time)
            time.sleep(self.time)
            node1 = node2
            if node1 == self.start:
                break;
            
    def InsertPriority(self,node):
		l = 0
		r = len(self.open)-1
		mid = 0
		while l <= r :
			mid = (l + r)/2
			if self.open[mid][1] > node[1]:
				r = mid - 1
			elif self.open[mid][1] < node[1]:
				l = mid + 1
			else:
				self.open.insert(mid,node)
				return
		if l < len(self.open):
			self.open.insert(mid,node)   
		else:
			self.open.append(node)
        #for i in range(len(self.open)):
		#	if self.open[i][1] >= node[1]:
		#		self.open.insert(i,node)
		#		return
        #self.open.append(node)

    def GetStraightWeight(self, node):
        return self.straight_distance[node]
    
    def Run(self):
        f = [0]*self.graph.number_of_nodes()
        while True:
            if len(self.open) == 0:
                return False
                
            selection = self.MinInOpen()
            
            self.close.append(selection)
            
            if self.ui_updater != None:
                self.ui_updater.ColorNode([selection[0]], self.time, 'g', 400)
                
            if selection[0] in self.goal:
                self.DrawPath()
                return True
                
            successors = [x for x in self.GetSuccessorsOf(selection[0])]
            
            for m in successors:
                cost_selection = selection[1]
                cost_selection_m = self.GetEdgeWeight(selection[0], m)
                
                tmpCl = self.IsInClosed(m)
                tmpOp = self.IsInOpen(m)
                
                if tmpCl == None and tmpOp == None:
                    cost_m = cost_selection + cost_selection_m
                    
                    f[m] = cost_m + self.GetStraightWeight(m)
                  
                    
                    self.InsertPriority((m, cost_m))
                    if self.ui_updater != None:
                        if self.path_tracer[m] == -1:
                            self.path_tracer[m] = selection[0]
                        self.ui_updater.ColorNode([m], self.time, 'y', 350)
                        time.sleep(self.time)
                    #tmpOp = self.IsInOpen(m)
                
                fold = f[m]    
                if tmpCl != None or tmpOp != None:
                    if tmpCl != None:
                        cost_m = tmpCl[1]
                    else:
                        cost_m = tmpOp[1]
                    if cost_m > cost_selection + cost_selection_m:
                        cost_m = cost_selection + cost_selection_m
                    
                    f[m] = cost_m + self.GetStraightWeight(m)

                if f[m] < fold: 
                    if tmpCl != None:
                        self.InsertPriority((self.close.pop(self.close.index(tmpCl))[0], cost_m))
                    else:
                        self.InsertPriority((self.open.pop(self.open.index(tmpOp))[0], cost_m))
                    if self.ui_updater != None:
                        self.ui_updater.ColorNode([m], self.time, 'y', 350)
                        time.sleep(self.time)
                        self.path_tracer[m] = selection[0]
                else:
                    f[m] = fold
            print self.open
                    	