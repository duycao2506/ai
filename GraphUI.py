﻿import sys
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import Algorithms as al
import UCS as ucs
import A_star as astar
import IDS as ids
import time
import sys
import wx

class PathFinderGraph(object):
    """Path finder Graph UI"""
    gDGT = None
    pos = None
    start = -1
    end = -1
    algorithm = None
    hs = None
    isDirected = False

    def __init__(self,nodes,edges, algo,start,end,isDirected,hs = []):
        self.gDGT = nx.DiGraph(algorithm = algo)
        self.AddNodes(nodes)
        self.AddEdges(edges,nodes)
        
        self.start = start
        self.end = end 
        self.hs = hs;
            

    def AddNodes(self,nodes):
        for i in range(len(nodes)):
            self.gDGT.add_node(i,city = nodes[i])

    def AddEdges(self,edges,nodes):
        for x in nodes:
            for y in nodes:
                if y in edges[x]:
                    self.gDGT.add_edge(nodes.index(x),nodes.index(y),w = edges[x][y]['cost'])
                    #if self.gDGT.graph['algorithm'] == "A*":
                     #   self.gDGT.get_edge_data(nodes.index(x),nodes.index(y))['hs'] = edges[x][y]['hs']

    def DrawGraph(self,speed):
        self.pos = nx.shell_layout(self.gDGT,nlist = None,dim =2, scale = 2)
        nx.draw(self.gDGT,pos= self.pos)
        node_labels = nx.get_node_attributes(self.gDGT,'city')
        nx.draw_networkx_labels(G = self.gDGT,pos = self.pos, labels = node_labels)
        edge_labels = nx.get_edge_attributes(self.gDGT,'w')
        nx.draw_networkx_edge_labels(G = self.gDGT,pos = self.pos,labels = edge_labels, label_pos = 0.5,font_size = 6)
        
        # update real-time
        plt
        plt.show(block = False)
        
        self.algorithm = self.ChooseAlgorithm(self.start,self.end)
        self.SetSpeed(speed)
        time1 = time.time()
        self.algorithm.Run()
        #if foundornot==False:
        #    wx.MessageBox("NOT FOUND", "ALERT",  wx.OK | wx.ICON_WARNING)
        time2 = time.time()

        print '%s function took %0.3f ms' % (self.algorithm, (time2-time1)*1000.0)

    #Choosing the algorithm
    def ChooseAlgorithm(self,start,end):
        if self.gDGT.graph['algorithm'] == al.Algorithm.ALGOR[0]:
            return ucs.UCS(self.gDGT,start,end,self)
        elif self.gDGT.graph['algorithm'] == al.Algorithm.ALGOR[2]:
            return astar.A_star(self.gDGT,start,end,self,self.hs)
        else:
            return ids.IDS(self.gDGT,start,end,self)
        # to be continue

    #Tracepath from end to start
    def ColorPath(self,start,end,delay):
        nx.draw_networkx_edges(self.gDGT,pos = self.pos, edgelist = [(end,start)], edge_color = 'r', width = 2.0)
        if delay == 0 :
            return
        plt.pause(delay)
    

    # color : 'r' , 'b', 'g'
    def ColorNode(self,nodes,delay,color,size):
        nx.draw_networkx_nodes(self.gDGT, pos = self.pos, nodelist = nodes, node_color = color, node_size = size)
        if delay == 0 :
            return
        plt.pause(delay)

    def SetSpeed(self,speed):
        self.algorithm.ChangeSpeed((1500+1-speed)/1000.0)
