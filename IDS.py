﻿import sys
import Algorithms
import networkx as nx
import time

class IDS(Algorithms.Algorithm):
	path_tracer = None

	def __init__(self,graph,start,end,updater):
		super(IDS, self).__init__(graph,start,end,updater)
		self.path_tracer =  [-1]*graph.number_of_nodes()
		self.open = [start]
		self.start = start
		self.close = []
		self.graph = graph.to_undirected()
		self.goal = []
		self.goal.append(end)
		self.ui_updater = updater

	def GetSuccessorsOf(self,node):
		return nx.neighbors(self.graph,node)

	def DrawPath(self):
		node1 = self.goal[0]
		node2 = -2
		while node2 != -1:
			node2 = self.path_tracer[node1]
			self.ui_updater.ColorPath(node1,node2,self.time)
			time.sleep(self.time)
			node1 = node2
			if node1 == self.start:
				break
				
	def IsInClosed(self, node):
		for i in self.close:
			if i == node:
				return True
		return False

	# check if node is in open
	def IsInOpen(self,node):
		for i in self.open:
			if i == node:
				return True
		return False

	def DFS(self,i):
		for j in range(0,i):
			selection = self.open.pop()
			if (selection in self.goal) == True:
				return 0
			if self.IsInClosed(selection) == False:
				self.close.append(selection)
				
				self.ui_updater.ColorNode([selection],self.time,'g',350)
				time.sleep(self.time)
				success = self.GetSuccessorsOf(selection)
				if success == None and open == None:
					return -1
				for x in success:
					if self.IsInOpen(x) == False and self.IsInClosed(x) == False:
						self.open.append(x)
						self.ui_updater.ColorNode([x],self.time,'y',350)
						time.sleep(self.time)
		return 1

	def ProcessingResult(self):
		tmp = self.goal[0]
		i = len(self.close)-1
		while i >= 0:
			self.path_tracer[tmp] = self.close[i]
			tmp = self.close[i]
			i = i - 1
	def Reset(self):
		if len(self.close) == 0:
			return
		self.ui_updater.ColorNode(self.open,0,'r',350)      
		self.ui_updater.ColorNode(self.close,0,'r',350)
		self.close = []
		self.open =[self.start]
		self.ui_updater.ColorNode(self.open,0,'y',350)

	def Run(self):
		i = 0
		dfsCall = 1
		while dfsCall == 1:
			self.Reset()
			i = i + 1
			dfsCall = self.DFS(i)
				
		if dfsCall == 0:
			print self.close
			self.ProcessingResult()
			self.DrawPath()
			return True
		return False